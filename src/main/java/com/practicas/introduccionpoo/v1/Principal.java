package com.practicas.introduccionpoo.v1;

public class Principal {
	
	

	public static void main(String[] args) {
		
		// Se crea un objeto de tipo Perro con uno de los métodos constructores 
		// definidos en la clase Perro
		Perro perro = new Perro("Ruffo");
		
		// Ejecucion del comportamiento emitir sonido heredado de la super clase abstracta
		// e implementado en la clase Perro
		perro.emitirSonido();
		
		// Se crea una instancia de la clase Gato
		Mascota gato = new Gato("Pelusa");
		
		// Se envia el mensaje emitirSonido al objeto gato 
		gato.emitirSonido();
		
		// Utilizamos polimorfismo y almacenamos un objeto perro 
		// en una variable de tipo Animal
		Mascota a = perro;
		a.emitirSonido();
		
		// Sustituimos un objeto animal por un objeto gato (LSP)
		a = gato;
		a.emitirSonido();
		
		a = new GatoSiames("");
		a.emitirSonido();
		
		gato =  new GatoSiames("Nena");
		gato.emitirSonido();
		
		Propietario p = new Propietario("123", "German");
		gato.setPropietario(p);
		
		System.out.println("datos de la mscota : " + gato);

	}

}
