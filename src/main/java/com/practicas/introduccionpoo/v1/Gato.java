package com.practicas.introduccionpoo.v1;

public class Gato extends Mascota {
		
	public Gato(String nombre) {
		super(nombre);
	}


	public Gato(String nombre, boolean agresivo) {
		super(nombre, agresivo);
	}
	

	public Gato(String nombre, String genero, Integer edad) {
		super(nombre, genero, edad);
	}


	@Override
	public void emitirSonido() {
		System.out.println("miau miau");
	}


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.getNombre();
	}

	
}
