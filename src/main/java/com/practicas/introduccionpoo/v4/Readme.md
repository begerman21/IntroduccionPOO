# Repaso 

## Fundamentos de la POO
- Objeto y Clase.
- Campos(Estado) o Propiedades.
- Métodos o Comportamientos.
- Jerarquía de clase.

## Los pilares de la POO
- Abstracción.
- Polimorfismo.
- Encapsulación.
- Herencia.

## Relaciones entre Objetos
- Dependencia.
- Asociación.
- Agregación.
- Composición.
- Implementación.
- Herencia.

## Principios
- Principio de sustitución de Liskov (LSP).

# Historias de Usuarios
- **COMO** Secretaría **QUIERO** ver todas las mascotas de un cliente en particular.
- 

# Preguntas ?
- Como validamos si una jerarquia de clases es correcta ?
- O con que criterio armo una jerarquia de clases ?

