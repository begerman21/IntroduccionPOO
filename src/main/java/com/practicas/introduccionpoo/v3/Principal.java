package com.practicas.introduccionpoo.v3;

public class Principal {
	
	

	public static void main(String[] args) {
		
		// Se crea un objeto de tipo Perro con uno de los métodos constructores 
		// definidos en la clase Perro
		Mascota perro = new Perro("Ruffo");
		
		// Ejecucion del comportamiento emitir sonido heredado de la super clase abstracta
		// e implementado en la clase Perro
		perro.emitirSonido();
		
		// Se crea una instancia de la clase Gato
		Mascota gato = new Gato();
		
		// Se envia el mensaje emitirSonido al objeto gato 
		gato.emitirSonido();
		
		// Utilizamos polimorfismo y almacenamos un objeto perro 
		// en una variable de tipo Animal
		Mascota a = perro;
		a.emitirSonido();
		
		// Sustituimos un objeto animal por un objeto gato (LSP)
		a = gato;
		a.emitirSonido();
		
		a = new GatoSiames();
		a.emitirSonido();
		
		gato =  new GatoSiames();
		gato.emitirSonido();
		
		
		Propietario p = new Propietario("123","German", "begerman21@gmail", "3786615545");
		Propietario pp = new Propietario("13","German", "begerman21@gmail", "3786615545");
		
		perro.setPropietario(p);
		gato.setPropietario(pp);
		
		
		System.err.println(perro);
		System.err.println(gato);
		
		if(perro.tienenMismoPropietario(gato)) {
			System.out.print("son del mismo propietario");
		}else {
			System.err.print("No son del mismo propietario");
		}
	}
	
	

}
