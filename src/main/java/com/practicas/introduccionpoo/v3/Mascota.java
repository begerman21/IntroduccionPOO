package com.practicas.introduccionpoo.v3;

public abstract class Mascota {
	
	private String nombre;
	private String genero;
	private Integer edad;
	private Float peso;
	private String color;
	private Propietario propietario;
	
	/**
	 * Tipo de dato primitivo, boolean con minuscula.
	 * Es distinto de Boolean con mayuscula que es una Clase de Java.
	 */
	private boolean agresivo;
	
	
	public Mascota() {
		super();
	}

	public Mascota(String nombre) {
		super();
		this.nombre = nombre;
	}

	public Mascota(String nombre, String genero, Float peso) {
		super();
		this.nombre = nombre;
		this.genero = genero;
		this.peso = peso;
	}

	public Mascota(String nombre, String genero, Integer edad, Float peso, String color, boolean agresivo) {
		super();
		this.nombre = nombre;
		this.genero = genero;
		this.edad = edad;
		this.peso = peso;
		this.color = color;
		this.agresivo = agresivo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public Float getPeso() {
		return peso;
	}

	public void setPeso(Float peso) {
		this.peso = peso;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public abstract void emitirSonido();

	public boolean isAgresivo() {
		return agresivo;
	}

	public void setAgresivo(boolean agresivo) {
		this.agresivo = agresivo;
	}

	public Propietario getPropietario() {
		return propietario;
	}

	public void setPropietario(Propietario propietario) {
		this.propietario = propietario;
	}
	
	public boolean tienenMismoPropietario(Mascota m) {
		if(this.getPropietario().equals(m.getPropietario())) return true;
		else return false;
	}

	@Override
	public String toString() {
		return "[nombre : " + getNombre() + ", agresivo : " + isAgresivo() + ", propietario : "
				+ getPropietario() + "]";
	}
	
	
}
