package com.practicas.introduccionpoo.v3;

public class Perro extends Mascota{
	
	private Humano mejorAmigo;


	public Perro(String nombre, String genero, Float peso) {
		super(nombre, genero, peso);
		// TODO Auto-generated constructor stub
	}
	public Perro(String nombre, String genero, Integer edad, Float peso, String color, boolean agresivo) {
		super(nombre, genero, edad, peso, color, agresivo);
		// TODO Auto-generated constructor stub
	}
	public Perro(String nombre) {
		super(nombre);
		// TODO Auto-generated constructor stub
	}
	public Humano getMejorAmigo() {
		return mejorAmigo;
	}
	public void setMejorAmigo(Humano mejorAmigo) {
		this.mejorAmigo = mejorAmigo;
	}
	
	@Override
	public void emitirSonido() {
		System.out.println("gua gua gua");
		
	}
	@Override
	public String toString() {
		return "Perro " + super.toString();
	}


	
	
}
