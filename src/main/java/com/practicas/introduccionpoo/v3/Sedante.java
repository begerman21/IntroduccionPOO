package com.practicas.introduccionpoo.v3;

public class Sedante {

	private String nombre;

	public Sedante(String nombre) {
		super();
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/*
	 * Aqui se aplica Polimorfismo.
	 */
	public int getDosis(Mascota mascota) {
	
		if(mascota.isAgresivo()) {
			return 25; // dosis fuerte
		}else {
			return 10; // dosis por defecto
		}

	}
}

