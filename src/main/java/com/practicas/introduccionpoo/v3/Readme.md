# Refactoring 
- Renombrar la clase Animal por Mascota.

- Subir a la superclase la propiedad argel de gato y renombrar por agresivo para poder indicar cuando una mascota es agresiva independientemente si es o no gato.

- Ajustar los constructores de Gato y Perro para contemplar la nueva propiedad, definir al menos 4 constructores para cada clase. 


# Historias de Usuarios
- Como secretaria quiero poder registrar los datos de la mascota y su propietario.

- Como secretaria quiero que cuando no indique que una mascota es agresiva se guarde por defecto que justamente no es agresiva. 

- **COMO secretaria QUIERO poder ver los datos más relevantes de las mascotas y sus propietarios PARA contactarlos.** 

- **Como veterinario quiero poder determinar si dos mascotas son del mismo propietario.**

- **COMO Veterinario QUIERO poder consultar que mascota es agresiva PARA determinar la dosis de Sedante.**

#Eclipse
1. Extract superclase
2. Refactor Rename
3. Refactor Pull Up 

#Java
* this / super
* Método toString 
* Método equals
* Clase anonima 

#Extra
- TODO
- System.err
