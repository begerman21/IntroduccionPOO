package com.practicas.introduccionpoo.v3;

public class Gato extends Mascota {
	
	
	
	public Gato() {
		super();
		this.setAgresivo(false);
	}

	public Gato(String nombre) {
		super(nombre);
		super.setAgresivo(false);
	}

	@Override
	public void emitirSonido() {
		System.out.println("miau miau");
	}

	@Override
	public String toString() {
		return "Gato " + super.toString();
	}

	
}
