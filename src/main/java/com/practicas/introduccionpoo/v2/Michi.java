package com.practicas.introduccionpoo.v2;

public class Michi extends Mascota {
	
	private boolean argel;
	
	public Michi() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Michi(String nombre, String genero, Float peso) {
		super(nombre, genero, peso);
		// TODO Auto-generated constructor stub
	}

	public Michi(String nombre) {
		super(nombre);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void emitirSonido() {
		System.out.println("miau miau");
	}

	@Override
	public String toString() {
		return "Gato " + super.toString();
	}

	public boolean isArgel() {
		return argel;
	}

	public void setArgel(boolean argel) {
		this.argel = argel;
	}

	
}
