package com.practicas.introduccionpoo.v2;

public class Principal {
	
	

	public static void main(String[] args) {
		
		// Se crea un objeto de tipo Perro con uno de los métodos constructores 
		// definidos en la clase Perro
		Perro perro = new Perro("Ruffo");
		
		// Ejecucion del comportamiento emitir sonido heredado de la super clase abstracta
		// e implementado en la clase Perro
		perro.emitirSonido();
		
		// Se crea una instancia de la clase Gato
		Mascota gato = new Michi();
		
		// Se envia el mensaje emitirSonido al objeto gato 
		gato.emitirSonido();
		
		// Utilizamos polimorfismo y almacenamos un objeto perro 
		// en una variable de tipo Animal
		Mascota a = perro;
		a.emitirSonido();
		
		// Sustituimos un objeto animal por un objeto gato (LSP)
		a = gato;
		a.emitirSonido();
		
		a = new GatoSiames();
		a.emitirSonido();
		
		gato =  new GatoSiames();
		gato.emitirSonido();
		
		Sedante s = new Sedante("xxx");
		perro.setAgresivo(true);
		
		Iguana iguana = new Iguana();
		
		System.out.println("Dosis de Sedante: " + s.getDosis(perro));
		System.out.println("Dosis de Sedante: " + s.getDosis(gato));
		System.out.println("Dosis de Sedante: " + s.getDosis(iguana));
	}
	
	

}
