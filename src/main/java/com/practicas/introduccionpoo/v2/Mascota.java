package com.practicas.introduccionpoo.v2;

public abstract class Mascota {
	
	private String nombre;
	private String genero;
	private Integer edad;
	private Float peso;
	private String color;
	private Propietario propietario;
	private boolean peligroso;
	
	
	public Mascota() {
		super();
	}

	public Mascota(String nombre) {
		super();
		this.nombre = nombre;
	}

	public Mascota(String nombre, String genero, Float peso) {
		super();
		this.nombre = nombre;
		this.genero = genero;
		this.peso = peso;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public Float getPeso() {
		return peso;
	}

	public void setPeso(Float peso) {
		this.peso = peso;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public abstract void emitirSonido();

	public Propietario getPropietario() {
		return propietario;
	}

	public void setPropietario(Propietario propietario) {
		this.propietario = propietario;
	}
	
	public boolean tienenMismoPropietario(Mascota m) {
		if(this.getPropietario().equals(m.getPropietario())) return true;
		else return false;
	}

	public boolean isPeligroso() {
		return peligroso;
	}

	public void setPeligroso(boolean peligroso) {
		this.peligroso = peligroso;
	}




	
	
}
