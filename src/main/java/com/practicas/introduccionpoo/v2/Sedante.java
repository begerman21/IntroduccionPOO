package com.practicas.introduccionpoo.v2;

public class Sedante {

	private String nombre;

	public Sedante(String nombre) {
		super();
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/*
	 * Code Smells
	 * Por no aplicar Polimorfismo 
	 */
	public int getDosis(Mascota mascota) {
		
		int dosis = 10; // dosis por defecto 
		boolean dosisFuerte = false;
		if(mascota instanceof Perro) {
			Perro perro = (Perro) mascota;
			if(perro.isAgresivo()) {
				dosisFuerte = true;
			}
		}else if(mascota instanceof Michi) {
			Michi gato = (Michi)mascota;
			if(gato.isArgel()) {
				dosisFuerte = true;
			}
		}else if(mascota instanceof Iguana) {
			Iguana iguana = (Iguana) mascota;
			if(iguana.isViolenta()) {
				dosisFuerte = true;
			}
		}
		if(dosisFuerte){
			dosis = 25; 
		}
		return dosis;
	}
}
