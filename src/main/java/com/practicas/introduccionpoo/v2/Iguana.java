package com.practicas.introduccionpoo.v2;

public class Iguana extends Mascota {
	
	private boolean violenta = true;

	@Override
	public void emitirSonido() {
		System.out.println("igau igua igua !...");
	}

	public Iguana() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Iguana(String nombre, String genero, Float peso) {
		super(nombre, genero, peso);
		// TODO Auto-generated constructor stub
	}

	public Iguana(String nombre) {
		super(nombre);
		// TODO Auto-generated constructor stub
	}

	public boolean isViolenta() {
		return violenta;
	}

	public void setViolenta(boolean violenta) {
		this.violenta = violenta;
	}

	
}
