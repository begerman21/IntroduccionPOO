package com.practicas.introduccionpoo.v2;

public class Perro extends Mascota{
	
	private Humano mejorAmigo;
	private boolean agresivo;


	public Perro(String nombre, String genero, Float peso) {
		super(nombre, genero, peso);
		// TODO Auto-generated constructor stub
	}
	public Perro(String nombre) {
		super(nombre);
		// TODO Auto-generated constructor stub
	}
	public Humano getMejorAmigo() {
		return mejorAmigo;
	}
	public void setMejorAmigo(Humano mejorAmigo) {
		this.mejorAmigo = mejorAmigo;
	}
	
	@Override
	public void emitirSonido() {
		System.out.println("gua gua gua");
		
	}
	
	public boolean isAgresivo() {
		return agresivo;
	}
	public void setAgresivo(boolean agresivo) {
		this.agresivo = agresivo;
	}
	@Override
	public String toString() {
		return "Perro " + super.toString();
	}


	
	
}
